import browser from 'webextension-polyfill';
import LastFmApi from './api/last-fm';
import { autoCreatePlaylist } from './automation';

const SELECTORS = {
  contextMenuId: 'popper-portal',
  activeRow: '.datagrid-action.is-active',
  contextMenuList: '#popper-portal ul',
  contextMenuItemClass: '_2v5_R',
  contextMenuItemIconClasses: (icon) => ['_23e7I', 'icon', `icon-${icon}`],
  contextMenuItemLabelClass: '_1FfV3',
  trackWithContextMenu: '.datagrid-row.has-contextmenu',
  trackName: '[itemprop="name"]',
  trackArtist: '[itemprop="byArtist"]',
}

const lastFm = new LastFmApi(process.env.LAST_FM_API_KEY);

async function getSimilarTracks(track) {
  const response = await lastFm.getSimilarTracks(track);
  return response.track;
}

function getCurrentTrack() {
  console.log('extracting track from title', document.title);
  const [name, artist] = document.title.split('-');
  return {
    name: name.trim(),
    artist: artist.trim(),
  };
}

function getTrack() {
  const trackRowEl = document.querySelector(SELECTORS.trackWithContextMenu);
  /** @type {HTMLElement} */
  const nameEl = trackRowEl.querySelector(SELECTORS.trackName);
  /** @type {HTMLElement} */
  const artistEl = trackRowEl.querySelector(SELECTORS.trackArtist);
  const name = nameEl.innerText;
  const artist = artistEl.innerText;
  return {
    name: name.trim(),
    artist: artist.trim(),
  };
}

async function downloadSimilarTracks(track, similarTracks) {
  let csv = '';
  for (const similarTrack of similarTracks) {
    const row = [similarTrack.name, similarTrack.artist.name];
    csv += `${row.join(',')}\n`;
  }
  await browser.runtime.sendMessage({
    command: 'download',
    type: 'csv',
    filename: `Songs similar to ${track.name} by ${track.artist}`,
    data: csv,
  });
}

async function downloadSimilarSongs() {
  try {
    const track = getTrack();
    // remove other artists since it breaks last fm
    const [singleArtist] = track.artist.split(',');
    const similarTracks = await getSimilarTracks({ name: track.name, artist: singleArtist });
    await downloadSimilarTracks(track, similarTracks);

    // const similarTracks = [];
    // await autoCreatePlaylist(`Songs similar to ${track.name} by ${track.artist}`, similarTracks);
  } catch (error) {
    console.error(error);
  }
}

function addContextMenuButton(icon, label) {
  const menuList = document.querySelector(SELECTORS.contextMenuList);

  const iconSpan = document.createElement('span');
  iconSpan.classList.add(...SELECTORS.contextMenuItemIconClasses(icon));

  const labelSpan = document.createElement('span');
  labelSpan.classList.add(SELECTORS.contextMenuItemLabelClass);
  labelSpan.innerText = label;

  const button = document.createElement('button');
  button.classList.add(SELECTORS.contextMenuItemClass);
  button.type = 'button';
  button.appendChild(iconSpan);
  button.appendChild(labelSpan);

  const listItem = document.createElement('li');
  listItem.appendChild(button);

  menuList.appendChild(listItem);

  return button;
}

function contextMenuOpened() {
  const button = addContextMenuButton('download', 'Download similar songs playlist');
  button.addEventListener('click', () => {
    downloadSimilarSongs();
    // Close context menu by clicking the button that opened it
    document.querySelector(SELECTORS.activeRow).click();
  });
}

const observer = new MutationObserver((mutationList) => {
  for (const mutation of mutationList) {
    if (mutation.type === 'childList') {
      for (const addedNode of mutation.addedNodes) {
        if (addedNode.id === SELECTORS.contextMenuId) {
          contextMenuOpened();
        }
      }
    }
  }
});

const target = document.querySelector('body');
observer.observe(target, {
  childList: true,
  subtree: true,
});
