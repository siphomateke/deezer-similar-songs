import pkg from '../../package.json';

export default async function basicRequest({ baseUrl, params, method = 'get' }) {
  const url = new URL(baseUrl);
  url.search = new URLSearchParams(params).toString();
  const response = await fetch(url.toString(), { 
    method, 
    headers: {
      'User-Agent': `Similar Song Finder (v${pkg.version})`
    } 
  });
  const data = await response.json();
  return data;
}
