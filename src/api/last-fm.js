import basicRequest from './utils';

export default class LastFmApi {
  constructor(key, userAgent) {
    this.key = key;
    this.userAgent = userAgent;
  }

  /**
   *
   * @param {Object.<string, any>} params
   * @param {string} name Property of the response that contains the actual data.
   */
  async request(params, name) {
    Object.assign(params, {
      api_key: this.key,
      format: 'json',
    });
    const baseUrl = 'https://ws.audioscrobbler.com/2.0/';
    const data = await basicRequest({ baseUrl, params });
    if (data.error) throw new Error(data.message);
    return data[name];
  }

  getSimilarTracks(track, limit = 10) {
    return this.request({
      method: 'track.getSimilar',
      track: track.name,
      artist: track.artist,
      limit,
      autocorrect: 1,
    }, 'similartracks');
  }
}
