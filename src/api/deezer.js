import basicRequest from './utils';

class DeezerApi {
  /**
   *
   * @param {string} route
   * @param {Object.<string, any>} params
   * @param {'get'|'post'|'delete'} method
   */
  async request(route, params, method) {
    Object.assign(params, {
      format: 'json',
    });
    const baseUrl = 'https://api.deezer.com/';
    return basicRequest({
      baseUrl: baseUrl + route,
      params,
      method,
    });
  }

  createPlaylist() {
    this.request('user/me/playlists', {
      request_method: 'post',
    }, 'post');
  }
}
