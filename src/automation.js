function getPlaylistLink() {
  const sidebarLinks = document.querySelectorAll('.sidebar-nav-item .sidebar-nav-link');
  for (const link of sidebarLinks) {
    if (link.href.match(/\/.+?\/profile\/\d+\/playlists/) !== null) {
      return link;
    }
  }
  throw new Error('Failed to find playlist sidebar link');
}

class WaitForElementObserver {
  constructor() {
    this.target = document.querySelector('body');
    this.queue = [];
    this.observer = new MutationObserver((mutationList) => {
      for (const mutation of mutationList) {
        if (mutation.type === 'childList') {
          for (const addedNode of mutation.addedNodes) {
            this.mutationChanged(addedNode);
          }
        }
      }
    });
  }

  observe() {
    this.observer.observe(this.target, {
      childList: true,
      subtree: true,
    });
  }

  disconnect() {
    this.observer.disconnect();
  }

  mutationChanged(addedNode) {
    for (let i = this.queue.length - 1; i >= 0; i--) {
      const item = this.queue[i];
      if (addedNode.matches(item.selector)) {
        item.callback(addedNode);
        this.queue.splice(i, 1);
      }
    }
  }

  async waitForElement(selector) {
    return new Promise((resolve) => {
      this.queue.push({
        selector,
        callback: resolve,
      });
    });
  }
}

export const observer = new WaitForElementObserver();
observer.observe();

async function waitForElement(selector, changeSelector) {
  // Wait for change selector to load
  await observer.waitForElement(changeSelector);

  // Make sure element exists
  const el = document.querySelector(selector);
  if (el === null) {
    // If it doesn't, the page probably hasn't loaded.
    return waitForElement(selector, changeSelector);
  }
  return el;
}

// TODO: Timeout
function waitForAddPlaylistButton() {
  return waitForElement('button.create-assistant-container', '#page_profile, div[role="tabpanel"]');
}

function waitForCreatePlaylistModal() {
  const selector = '.modal-create-assistant';
  return waitForElement(selector, `${selector},.modal-dialog`);
}

// eslint-disable-next-line import/prefer-default-export
export async function autoCreatePlaylist(playlistName, tracks) {
  const playlistLink = getPlaylistLink();
  playlistLink.click();
  const addPlaylistButton = await waitForAddPlaylistButton();
  addPlaylistButton.click();
  const createPlaylistModal = await waitForCreatePlaylistModal();
  // FIXME: Make sure elements exist
  const playlistNameInput = createPlaylistModal.querySelector('.assistant-info .form-control.form-control-block');
  playlistNameInput.value = playlistName;
  playlistNameInput.dispatchEvent(new Event('change'));
  playlistNameInput.dispatchEvent(new Event('input'));

  setTimeout(() => {
    // const modalCreateButton = createPlaylistModal.querySelector('#modal_playlist_assistant_submit');
    // modalCreateButton.click();
  });
}
