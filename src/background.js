import browser from 'webextension-polyfill';
const downloadTypes = ['csv', 'json'];

browser.runtime.onMessage.addListener((message, sender) => {
  if (sender.id === browser.runtime.id) {
    if (message.command === 'download' && downloadTypes.includes(message.type)) {
      const blob = new Blob([message.data], { type: `application/${message.type}` });
      const url = URL.createObjectURL(blob);
      browser.downloads.download({
        url,
        filename: `${message.filename}.${message.type}`,
      });
    }
  }
});
